package com.co.pragma.crecimiento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class GatewayPracticaCrecimientoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewayPracticaCrecimientoApplication.class, args);
	}

}
